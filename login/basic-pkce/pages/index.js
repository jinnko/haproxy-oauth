import Head from 'next/head'
import styles from '../styles/Home.module.css'

import {createHash} from 'crypto';

const randomString = (length = 8) => {
  // Declare all characters
  let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._~';

  // Pick characers randomly
  let str = '';
  for (let i = 0; i < length; i++) {
      str += chars.charAt(Math.floor(Math.random() * chars.length));
  }

  return str;
};

const oauth2pkce = async () => {
  const clientId = process.env.CLIENT_ID
  const redirectUrl = process.env.REDIRECT_URL
  const requestScope = "read_user openid email"

  const responseType = 'code' // code | token (doesn't work)

  const state = randomString(45) // prevents CSRF attacks
  //const codeVerifier = new TextEncoder().encode(randomString(128))
  // TODO: This needs to be unpredictable
  const codeVerifier = new TextEncoder().encode(process.env.CODE_VERIFIER)
  const hashBuffer = await window.crypto.subtle.digest('SHA-256', codeVerifier)
  const hashArray = Array.from(new Uint8Array(hashBuffer))
  const hashBin = await hashArray.map(b => String.fromCharCode(b)).join('')
  const codeChallenge = btoa(hashBin).replace(/\+/g, '-').replace(/\//g, '_').replace(/[=]+$/, '')

  // See https://gitlab.com/.well-known/openid-configuration for available options
  const URL = `https://gitlab.com/oauth/authorize?client_id=${clientId}&redirect_uri=${redirectUrl}&response_type=${responseType}&state=${state}&scope=${requestScope}&code_challenge=${codeChallenge}&code_challenge_method=S256`

  document.location.href = URL
}

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Login</title>
        <meta name="description" content="Login" />
      </Head>

      <main className={styles.main}>
        {/* <h3 className={styles.title}>
          Select a login provider
        </h3> */}

        <p className={styles.description}>
          <button onClick={oauth2pkce}>Gitlab OAuth2 PKCE</button>
        </p>


      </main>

      {/* <footer className={styles.footer}>
        Powered by Ixydo
      </footer> */}
    </div>
  )
}
