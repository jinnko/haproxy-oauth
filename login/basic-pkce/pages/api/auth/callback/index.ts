import fetch, { Response, BodyInit } from 'node-fetch';
import jwt from 'jsonwebtoken'
import jwksClient from 'jwks-rsa'
import Cookies from 'cookies'

const clientId = process.env.CLIENT_ID;
const clientSecret = process.env.CLIENT_SECRET;

type oauthToken = {
    access_token: string
    token_type: string
    expires_in: number
    refresh_token: string
    scope: string
    created_at: number
    id_token?: string
}

type tokenInfoApplication = {
    uid: string
}

type tokenInfo = {
    resource_owner_id: number
    scope: string[]
    expires_in: number
    application: tokenInfoApplication
    created_at: number
    scopes: string[]
    expires_in_seconds: number
}

type jwtToken = {
    iss: string
    sub: string
    aud: string
    exp: number
    iat: number
    auth_time: number
    sub_legacy: string
    email: string
    email_verified: boolean
    groups_direct: string[]
}

type OpenIDConfiguration = {
    issuer: string
    authorization_endpoint: string
    token_endpoint: string
    revocation_endpoint: string
    introspection_endpoint: string
    userinfo_endpoint: string
    jwks_uri: string
    scopes_supported: string[]
    response_types_supported: string[]
    response_modes_supported: string[]
    grant_types_supported: string[]
    token_endpoint_auth_methods_supported: string[]
    subject_types_supported: string[]
    id_token_signing_alg_values_supported: string[]
    claim_types_supported: string[]
    claims_supported: string[]
}

type OpenIDKey = {
    kty: string
    kid: string
    e: string
    n: string
    use: string
    alg: string
}

type OpenIDKeys = {
    keys: OpenIDKey[]
}

const randomString = (length = 8): string => {
    // Declare all characters
    let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._~';

    // Pick characers randomly
    let str = '';
    for (let i = 0; i < length; i++) {
        str += chars.charAt(Math.floor(Math.random() * chars.length));
    }

    return str;
}

/**
 * Gets the OpenId Configuration, including public signing certificate
 * returns { oidConf, oidKeys }
 */
const getSigningKey = async (): Promise<string> => {
    const url = "https://gitlab.com/.well-known/openid-configuration"
    const res_oidconf = await fetch(url)

    if (!res_oidconf.ok) {
        throw new Error(await res_oidconf.text())
    }

    const oidConf = await res_oidconf.json() as OpenIDConfiguration

    // Discover the key id
    const { jwks_uri } = oidConf
    const res_jwks_uri = await fetch(jwks_uri)

    if (!res_jwks_uri.ok) {
        throw new Error(await res_jwks_uri.text())
    }

    const {keys} = await res_jwks_uri.json() as OpenIDKeys

    // Get the actual certificate
    const client_jwks = jwksClient({
        jwksUri: oidConf.jwks_uri
    })
    const key = await client_jwks.getSigningKey(keys[0].kid)
    const signingKey = key.getPublicKey()

    return signingKey
}

/**
 * Use the received code & state to request a auth token and jwt
 * @param code The returned code from the identity provider
 * @returns response
 */
const getToken = async (code: string): Promise<Response> => {
    // TODO - the verifier needs to be randomized, but consistent between the request and the callback
    //const codeVerifier = randomString(128);
    const codeVerifier = process.env.CODE_VERIFIER

    const params = new URLSearchParams();
    params.append('client_id', clientId);
    params.append('client_secret', clientSecret)
    params.append('code', code)
    params.append('grant_type', 'authorization_code')
    params.append('redirect_uri', process.env.REDIRECT_URL)
    params.append('code_verifier', codeVerifier)

    const response = await fetch('https://gitlab.com/oauth/token', {
        method: 'post',
        body: (params as BodyInit)
    });

    if (!response.ok) {
        throw response
    }

    return response
}

const getTokenInfo = async(token: string): Promise<tokenInfo> => {
    const response = await fetch('https://gitlab.com/oauth/token/info', {
        headers: {
            Authorization: `Bearer ${token}`
                }
    })

    if (!response.ok) {
        throw response
    }

    return await response.json() as tokenInfo
}

const verifyToken = async(id_token: string): Promise<jwtToken> => {
    const options = {
        algorithms: "RS256",
        issuer: "https://gitlab.com",
        audience: clientId,
    }
    try {
        const signingKey = await getSigningKey()
        return jwt.verify(id_token, signingKey, options)
    } catch (e) {
        return e
    }
}

/**
 * WIP Refresh the tokens
 * @param json
 */
const refreshToken = async(json) => {
    const params = new URLSearchParams();
    params.append('client_id', process.env.CLIENT_ID)
    params.append('client_secret', process.env.CLIENT_SECRET)
    params.append('refresh_token', 'REFRESH_TOKEN')
    params.append('grant_type', 'refresh_token')
    params.append('redirect_uri', process.env.REDIRECT_URL)
    params.append('code_verifier', process.env.CODE_VERIFIER)
}


export default async function handler(req, res) {
    let response: Response
    try {
        response = await getToken(req.query.code)
    } catch (e) {
        res.status(e.status).json({response: await e.statusText})
        return
    }

    const json = await response.json() as oauthToken

    const tokenInfo = await getTokenInfo(json.access_token)
    const verified = await verifyToken(json.id_token)
    const signingKey = await getSigningKey()

    // Set cookie
    const cookieSigningKeys = [process.env.COOKIE_SIGNING_KEY]
    const cookies = new Cookies(req, res, {
        secure: true,
        keys: cookieSigningKeys,
    })
    cookies.set('id_token', json.id_token, {
        httpOnly: true,
        overwrite: true,
        signed: true,
    })

    try {
        res.status(200).json({ req_query: req.query, json, tokenInfo, verified, signingKey })
        return
    } catch(e) {
        res.status(400).json({e})
        return
    }
}
